const path =  require('path');
// const nodeMailer = require('nodemailer');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();

//controllers
const indexController = require('./controllers/index');
const aboutController = require('./controllers/about');
const blogController = require('./controllers/blog');
const contactController = require('./controllers/contact');
const servicesController = require('./controllers/services');
const worksController = require('./controllers/works');
const workController = require('./controllers/work');


// set the template engine
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use('/', express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');

app.get('/', indexController);
app.get('/about', aboutController);
app.get('/blog', blogController);
app.get('/contact', contactController);
app.get('/services', servicesController);
app.get('/works', worksController);
app.get('/work', workController);

app.listen(4800, console.log('server connected on port 4800!'));





